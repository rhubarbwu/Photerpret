# Visual-Recognition-Demo
Project Photerpret for QHacks 2018

Scans through folders of product images and outputs various information, such as nutrition information and ingredients.
Run the program using urlLoader.py (manual folder entry) or urlLoaderC.py (supports command line execution and text file export).
