########### Python 3.6 #############
import requests, base64

headers = {
    # Request headers.
    'Content-Type': 'application/octet-stream',

    # NOTE: Replace the "Ocp-Apim-Subscription-Key" value with a valid subscription key.
    'Ocp-Apim-Subscription-Key': 'd7b0ddbee99f44809584281a7f474115',
}

params = {
    # Request parameters. All of them are optional.
    'visualFeatures': 'Categories',
    'details': 'Celebrities', 
    'language': 'en',
}

# Replace the three dots below with the full file path to a JPEG image of a celebrity on your computer or network.
image = open('C:\CS\QHacks2018\Visual-Recognition-Demo\\00034000002467_NF__master.jpg','rb').read() # Read image file in binary mode

try:
    # NOTE: You must use the same location in your REST call as you used to obtain your subscription keys.
    #   For example, if you obtained your subscription keys from wes
    response = requests.post(url = 'https://eastus.api.cognitive.microsoft.com/vision/v1.0/ocr',
                             headers = headers,
                             params = params,
                             data = image)
    data = response.json()
    print(data)
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))
####################################